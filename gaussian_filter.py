# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:38:00 2016
"""

import numpy as np


    
def separable_deriv_gauss_kernel( sigma, size=7):
    halfsize = np.floor(size/2)
    k1 = np.zeros((size))
    k2 = np.zeros((size))
    for i in xrange(size):
        u = i-halfsize-1
        gauss_1 = np.exp(-u**2/(2*sigma**2)) / (sigma*np.sqrt(2*np.pi))
        dgauss_2 = -u * gauss_1 / sigma**2
        k2[i] = gauss_1
        k1[i] = dgauss_2
    return k1,k2
    
def fast_2D_convolve(image, k1, k2):
    n = len(image)
    aux = np.zeros((n,n))
    for i in xrange(n):
        aux[i,:] = np.convolve(image[i,:],k1, 'same')
    for i in xrange(n):
        aux[:,i] = np.convolve(aux[:,i], k2, 'same')  
    return aux
        

