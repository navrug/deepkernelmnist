# -*- coding: utf-8 -*-
"""
Created on Tue Mar  1 15:53:18 2016

@author: Ronan
"""

import numpy as np
from svm_classifier import svm_classifier


#%% Outputs: comes from https://gist.github.com/mblondel/586753
import pylab as pl

def gen_lin_separable_data():
    # generate training data in the 2-d case
    mean1 = np.array([0, 2])
    # navrug is gay
    mean2 = np.array([2, 0])
    cov = np.array([[0.8, 0.6], [0.6, 0.8]])
    X1 = np.random.multivariate_normal(mean1, cov, 100)
    y1 = np.ones(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov, 100)
    y2 = np.ones(len(X2)) * -1
    return X1, y1, X2, y2

def gen_non_lin_separable_data():
    mean1 = [-1, 2]
    mean2 = [1, -1]
    mean3 = [4, -4]
    mean4 = [-4, 4]
    cov = [[1.0,0.8], [0.8, 1.0]]
    X1 = np.random.multivariate_normal(mean1, cov, 50)
    X1 = np.vstack((X1, np.random.multivariate_normal(mean3, cov, 50)))
    y1 = np.ones(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov, 50)
    X2 = np.vstack((X2, np.random.multivariate_normal(mean4, cov, 50)))
    y2 = np.ones(len(X2)) * -1
    return X1, y1, X2, y2

def gen_lin_separable_overlap_data():
    # generate training data in the 2-d case
    mean1 = np.array([0, 2])
    mean2 = np.array([2, 0])
    cov = np.array([[1.5, 1.0], [1.0, 1.5]])
    X1 = np.random.multivariate_normal(mean1, cov, 100)
    y1 = np.ones(len(X1))
    X2 = np.random.multivariate_normal(mean2, cov, 100)
    y2 = np.ones(len(X2)) * -1
    return X1, y1, X2, y2

def split_train(X1, y1, X2, y2):
    X1_train = X1[:90]
    y1_train = y1[:90]
    X2_train = X2[:90]
    y2_train = y2[:90]
    X_train = np.vstack((X1_train, X2_train))
    y_train = np.hstack((y1_train, y2_train))
    return X_train, y_train

def split_test(X1, y1, X2, y2):
    X1_test = X1[90:]
    y1_test = y1[90:]
    X2_test = X2[90:]
    y2_test = y2[90:]
    X_test = np.vstack((X1_test, X2_test))
    y_test = np.hstack((y1_test, y2_test))
    return X_test, y_test

def plot_contour(X1_train, X2_train, clf):
    pl.plot(X1_train[:,0], X1_train[:,1], "ro")
    pl.plot(X2_train[:,0], X2_train[:,1], "bo")
    pl.scatter(clf.support_vectors_[:,0],clf.support_vectors_[:,1],s=100,c="g")

    X1, X2 = np.meshgrid(np.linspace(-6,6,50), np.linspace(-6,6,50))
    X = np.array([[x1, x2] for x1, x2 in zip(np.ravel(X1), np.ravel(X2))])
    Z = clf.decision_function(X).reshape(X1.shape)
    pl.contour(X1, X2, Z, [0.0], colors='k', linewidths=1, origin='lower')
    pl.contour(X1, X2, Z + 1,[0.0],colors='grey',linewidths=1,origin='lower')
    pl.contour(X1, X2, Z - 1,[0.0],colors='grey',linewidths=1,origin='lower')

    pl.axis("tight")
    pl.show()
    
    
def shuffle(X1, y1, X2, y2):
    ind = range(len(y1))
    np.random.shuffle(ind)
    X1 = X1[ind]
    y1 = y1[ind]
    X2 = X2[ind]
    y2 = y2[ind]    
    return X1, y1, X2, y2

def test_linear(C, degree, offset, solver):
    X1, y1, X2, y2 = gen_lin_separable_data()
    X1, y1, X2, y2 = shuffle(X1, y1, X2, y2)
    
    X_train, y_train = split_train(X1, y1, X2, y2)
    X_test, y_test = split_test(X1, y1, X2, y2)

    clf = svm_classifier(C=C, degree=degree, offset=offset, solver=solver)
    clf.fit(X_train, y_train)
 
    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print "%d out of %d predictions correct" % (correct, len(y_predict))

    plot_contour(X_train[y_train==1], X_train[y_train==-1], clf)

def test_non_linear(C, degree, offset, solver):
    X1, y1, X2, y2 = gen_non_lin_separable_data()
    X1, y1, X2, y2 = shuffle(X1, y1, X2, y2)
    
    X_train, y_train = split_train(X1, y1, X2, y2)
    X_test, y_test = split_test(X1, y1, X2, y2)


    clf = svm_classifier(C=C, degree=degree, offset=offset, solver=solver)
    clf.fit(X_train, y_train)

    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print "%d out of %d predictions correct" % (correct, len(y_predict))

    plot_contour(X_train[y_train==1], X_train[y_train==-1], clf)

def test_soft(C, degree, offset, solver):
    X1, y1, X2, y2 = gen_lin_separable_overlap_data()
    X1, y1, X2, y2 = shuffle(X1, y1, X2, y2)
    
    X_train, y_train = split_train(X1, y1, X2, y2)
    X_test, y_test = split_test(X1, y1, X2, y2)

    clf = svm_classifier(C=C, degree=degree, offset=offset, solver=solver)
    clf.fit(X_train, y_train)

    y_predict = clf.predict(X_test)
    correct = np.sum(y_predict == y_test)
    print "%d out of %d predictions correct" % (correct, len(y_predict))

    plot_contour(X_train[y_train==1], X_train[y_train==-1], clf)
 
#%%

solver = 'smo' 
 
# This should work
#np.random.seed(0)
test_linear(C=1, degree=1, offset=0, solver=solver)
test_non_linear(C=1, degree=3, offset=1, solver=solver)
test_soft(C=1, degree=1, offset=0, solver=solver)

# This should not work
test_non_linear(C=1, degree=1, offset=0, solver=solver)
