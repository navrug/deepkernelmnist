# -*- coding: utf-8 -*
"""
Created on Mon Feb 29 15:24:53 2016

@author: Ronan
"""
import numpy as np
import sys
import cvxopt

from scipy.spatial.distance import cdist, pdist, squareform

#%%
class svm_classifier(object):
    def __init__(self,
                 C = 1.5,
                 degree = 1,
                 offset = 1,
                 gamma = 1.0, #exponential kernel parameter
                 mix = None,
                 solver = None,
                 kernel = "poly",
                 thresh = 1e-10,
                 max_iterations = 1000,
                 verbose=False):
        self.C = C
        self.degree = degree
        self.offset = offset
        self.gamma  = gamma
        if mix is None:
            self.mix = 2**(-degree)
        self.kernel = kernel
        self.eps = 1e-6
        self.tol = 1e-3
        self.K = None
        self.error = None
        self.alpha = None
        self.b = 0
        self.support_vectors_ = None
        self.support_alphas_ = None
        self.support_labels_ = None
        self.support_ = None # Indices of support vectors
        self.solver = solver
        self.thresh = thresh
        self.max_iterations = max_iterations
        self.verbose = verbose
        
        
    # TODO: Try a vectorial version
    def _get_error(self, X, y, i):
        if not (self.alpha[i] > 0) & (self.alpha[i] < self.C):
            self.error[i] = np.sum(self.alpha*y*self.K[i,:]) - self.b - y[i]
        return self.error[i]
        
     
    def _SMO(self, X, y):
        self.error = - y.astype(float)
        self.b = 0
        num_changed = 0
        examine_all = True
        it = 0
        while it < self.max_iterations and (num_changed > 0 or examine_all):
            num_changed = 0
            if examine_all:
                for i in xrange(len(X)):
                    num_changed += self._examine_example(X,y,i)
            else:
                on_bound = (self.alpha>self.eps) & (self.alpha<self.C-self.eps)
                for i in np.nonzero(on_bound)[0]:
                    num_changed += self._examine_example(X,y,i)
            if examine_all:
                examine_all = False
            elif num_changed == 0:
                examine_all = True
            it += 1
        if it == self.max_iterations:
            print "WARNING: Reached max iterations."

    def _examine_example(self, X, y, i2):
        E2 = self._get_error(X,y,i2)
        r2 = E2*y[i2]
        # Check whether alpha_2 violates KKT
        if (r2 < -self.tol and self.alpha[i2] < self.C) or \
            (r2 > self.tol and self.alpha[i2] > 0):
            on_bound = (self.alpha > 0) & (self.alpha < self.C)
            # Try to make progress with the heuristic best
            if np.sum(on_bound) > 1:
                i1 = np.argmin(np.abs(self.error - E2))
                if i1 != i2 and self._take_step(X,y,i1,i2):
                    return 1
            # Otherwise just try with all non-bound samples
            # TODO: Make this constant time
            on_bound_idx = np.nonzero(on_bound)[0]
            if len(on_bound_idx) > 0:
                start = np.random.randint(len(on_bound_idx))
                for i1 in np.hstack([on_bound_idx[start:], 
                                     on_bound_idx[:start]]):
                    if i1 != i2 and self._take_step(X,y,i1,i2):
                        return 1
            # Otherwise try with any sample
            start = np.random.randint(len(y))
            for i1 in range(start, len(y)) + range(start):
                if i1 != i2 and self._take_step(X,y,i1,i2):
                    return 1
        return 0
                
    def _take_step(self, X, y, i1, i2):
        if (i1 == i2):
            return False
        y1 = y[i1]
        y2 = y[i2]
        E1 = self._get_error(X,y,i1)
        E2 = self._get_error(X,y,i2)  
        alph1 = self.alpha[i1]
        alph2 = self.alpha[i2]
        s = y1 * y2
        L,H = self._compute_LH(y, i1, i2)    
        if (L==H):
            return False 
        eta = 2 * self.K[i1, i2] - self.K[i1, i1] - self.K[i2, i2]     
        #compute Lobj, Hobj, objective function at a2=L, a2=H
        c1 = eta/2
        c2 = y2 * (E1 - E2) - eta * alph2
        Lobj = c1 * L * L + c2 * L
        Hobj = c1 * H * H + c2 * H
        if (eta<0):
            a2 = alph2 + y2 * (E2 - E1) / eta
            if (a2 < L):
                a2 = L
            elif (a2 > H):
                a2 = H     
        else:
            if (Lobj > Hobj + self.eps):
                a2 = L
            elif (Lobj < Hobj - self.eps):
                a2 = H
            else:
                a2 = alph2
        if (np.abs(a2-alph2) < self.eps*(a2 + alph2+self.eps)):
            return False       
        a1 = alph1 - s*(a2-alph2)    
        if (a1 < 0):
            a2 += s * a1
            a1 = 0
        elif (a1 > self.C):
            t = a1 - self.C
            a2 += s * t
            a1 = self.C
        self.alpha[i1] = a1
        self.alpha[i2] = a2
        # Update threshold
        if a1 > 0 and a1 < self.C:
            b_new = self.b + E1 + y1*(a1-alph1)*self.K[i1, i1] \
                + y2*(a2-alph2)*self.K[i1, i2]
        else:
            if a2 > 0 and a2 < self.C:
                b_new = self.b + E2 + y2*(a2-alph2)*self.K[i2, i2] \
                    + y1*(a1-alph1)*self.K[i1, i2]
            else:
                b1 = self.b + E1 + y1*(a1-alph1)*self.K[i1, i1] \
                    + y2*(a2-alph2)*self.K[i1, i2]
                b2 = self.b + E2 + y2*(a2-alph2)*self.K[i2, i2] \
                    + y1*(a1-alph1)*self.K[i1, i2]
                b_new = 0.5*(b1+b2)
        delta_b = b_new - self.b
        self.b = b_new
        # Update error cache
        self.error += y[i1]*(a1-alph1)*self.K[i1,:] \
            + y[i2]*(a2-alph2)*self.K[i2,:] - delta_b
        self.error[i1] = 0
        self.error[i2] = 0
        return True

    def _compute_LH(self, y, i1, i2): 
        if (y[i1] == y[i2]):
            gamma = self.alpha[i1] + self.alpha[i2] 
            if (gamma > self.C):
                L = gamma - self.C 
                H = self.C 
            else:
                L = 0
                H = gamma 
        else:
            gamma = self.alpha[i1] - self.alpha[i2] 
            if (gamma > 0):
                L = 0
                H = self.C - gamma
            else:
                L = -gamma
                H = self.C
        return L,H

    def _solve_qp(self, X, y):
        ### cvxopt.solvers.qp() solves a QP of the following form:
        # min 1/2 x' P x + q' x
        # s.t.  Gx <= h
        #       Ax = b
        N = X.shape[0]                
        P = self.K * np.outer(y,y)
        q = -np.matrix(np.ones(N)).reshape(N,1)
        G1 = np.eye(N)
        G2 = -np.eye(N)
        G = np.vstack((G1, G2))
        del G1, G2      
        h1 = np.matrix(np.ones(N)*self.C).reshape(N,1)
        h2 = np.matrix(np.zeros(N)).reshape(N,1)
        h = np.vstack((h1, h2))
        del h1, h2      
        A = np.matrix(y).reshape(1, N)

        P = cvxopt.matrix(P)
        q = cvxopt.matrix(q)
        G = cvxopt.matrix(G)
        h = cvxopt.matrix(h)
        A = cvxopt.matrix(A, tc='d')
        b = cvxopt.matrix(0, tc='d')    
        if not self.verbose:
            cvxopt.solvers.options['show_progress'] = False
        self.alpha = np.ravel(cvxopt.solvers.qp(P, q, G, h, A, b)['x'])
        
    def fit(self, X, y):
        """
        Fit the SVM to features X and labels y.
        """
        if self.kernel == "poly" :
            self.K = np.power(np.dot(X, X.T) + self.offset, self.degree)
        elif self.kernel == "rbf":
            self.K = np.exp(- self.gamma * squareform(pdist(X, 'sqeuclidean')))
        elif self.kernel == "mix":
            self.K = self.mix*np.power(np.dot(X, X.T) + self.offset, self.degree) \
                + np.exp(- self.gamma * squareform(pdist(X, 'sqeuclidean')))

        else:
            raise NameError("Kernel type not recognised.")
            
        self.alpha = np.zeros(len(y))
        
        ### Solve the dual, results in self.alpha
        if self.solver == 'smo':
            self._SMO(X,y)
        elif self.solver == 'cvxopt':
            self._solve_qp(X,y)
        ###
        del self.K
        sv = self.alpha > self.thresh       
        self.support_vectors_ = X[sv]
        self.support_alphas_ = self.alpha[sv]
        self.support_labels_ = y[sv]
        self.support_ = np.nonzero(sv)[0]
        
        ### If solver is 'cvxopt', the biais must be computed
        if self.solver == 'cvxopt':
            self.b = 0
            s_err = self.support_labels_ - self.predict(self.support_vectors_)
            self.b = np.mean(s_err)
            if self.verbose:
                print 's_err mean (->bias): %s, s_err std: %s' %(self.b,
                                                                 np.std(s_err))
                                   
    def decision_function(self, X):
        if self.kernel == "poly":
            res = np.dot(self.support_alphas_*self.support_labels_, 
                         np.power(np.dot(self.support_vectors_,X.T) \
                         + self.offset,self.degree)) - self.b    
        elif self.kernel == "rbf":
            res = np.dot(self.support_alphas_*self.support_labels_, 
                         np.exp(- self.gamma \
                         * cdist(self.support_vectors_, X, 'sqeuclidean').reshape(len(self.support_vectors_), len(X))))\
                         - self.b
        elif self.kernel == "mix":
            res = np.dot(self.support_alphas_*self.support_labels_, 
                         self.mix*np.power(np.dot(self.support_vectors_,X.T) \
                         + self.offset,self.degree)
                         + np.exp(- self.gamma \
                         * cdist(self.support_vectors_, X, 'sqeuclidean').reshape(len(self.support_vectors_), len(X))))\
                         - self.b
        else:
            raise NameError("Kernel type not recognised.")    
                         
        return res
            
    def predict(self, X):
        """
        Computes the SVM prediction on the given x.
        """
        return np.sign(self.decision_function(X))
        
    def score(self, X, y):
        y_pred = self.predict(X)
        return np.mean(y == y_pred)
        
        
class svm_multiclass(object):
    def __init__(self,
                 n_classes,
                 C=1.5,
                 degree=1,
                 offset=1,
                 gamma=1,
                 solver=None,
                 kernel = "rbf",
                 thresh=1e-10,
                 max_iterations=1000,
                 verbose=False):
        self.n_classes = n_classes
        self.svms = []
        for i in xrange(n_classes-1):
            i_vs = []
            for j in xrange(i+1,n_classes):
                i_vs.append(svm_classifier(C=C, 
                                           degree=degree, 
                                           offset=offset,
                                           gamma=gamma,
                                           solver=solver,
                                           kernel= kernel,
                                           thresh=thresh,
                                           max_iterations=max_iterations))
            self.svms.append(i_vs)
        self.support_vectors_ = None
        self.support_ = None # Indices of support vectors

    
    def fit(self, X, y):
        if len(np.unique(y)) != self.n_classes:
            raise NameError("Number of classes does not match.")
        is_sv = np.zeros(len(X), dtype=bool)
        count = 0
        for i in xrange(self.n_classes-1):
            for j in xrange(i+1, self.n_classes):
                sys.stdout.write("\rAdvancement: %d%%" 
                    %(100*float(count)/(self.n_classes*(self.n_classes-1)/2)))
                sys.stdout.flush()
                idx = np.logical_or(y==i, y==j)
                y_ij = 2*(y[idx] == i)-1
                self.svms[i][j-i-1].fit(X[idx], y_ij)
                idx_int = np.nonzero(idx)[0]
                is_sv[idx_int[self.svms[i][j-i-1].support_]] = True
                count += 1
        self.support_ = np.nonzero(is_sv)[0]
#        self.support_vectors_ = X[is_sv,:]

    def vote(self, X):
        votes = np.zeros((len(X), self.n_classes))
        for i in xrange(self.n_classes-1):
            for j in xrange(i+1, self.n_classes):
                pred = self.svms[i][j-i-1].predict(X)
                votes[:,i] += (pred==1)
                votes[:,j] += (pred==-1)
        return votes
        
    def predict(self, X):
        return np.argmax(self.vote(X), axis=1)
        
    def score(self, X, y):
        y_pred = self.predict(X)
        return np.mean(y == y_pred)


