# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 18:33:39 2016

@author: Gurvan
"""

"""
File in which we put mini-scripts of data analysis.
"""

import matplotlib.pyplot as plt
import numpy as np
from util import rotate_right, mirror
import scipy

#%% Load data

X = np.loadtxt("Xtr.csv", delimiter=",")
y = np.loadtxt("Ytr.csv", delimiter=",", usecols=[1], skiprows=1)
for i in xrange(len(X)):
    X[i,:] = mirror(rotate_right(X[i,:].reshape(28,28))).flatten()
d = len(X[0,:])
X = X*(X>0)
for i in xrange(len(X)):
    norm = np.dot(X[i,:],X[i,:])
    X[i,:] /= np.sqrt(norm)


#%% Digit distribution

plt.hist(y)

#%% Show a list of inputs, along with its labels
def show(to_show):
    for i in to_show:
        plt.figure(figsize =(1,1))
        print "i=%d, y[i]=%d" %(i, y[i])
        plt.imshow(-X[i,:].reshape((28,28)), cmap=plt.cm.gray)
        plt.axis('off')
        plt.show()
    
to_show = range(100)
show(to_show)

#%% Plot a random sample and display it in all directions


i = np.random.randint(len(y))
print "Sample labelled as", y[i]
m = -X[i,:].reshape((28,28))
f, axarr = plt.subplots(2, 4)
for i in range(4):
    axarr[0,i].matshow(m, cmap=plt.cm.gray)
    m = rotate_right(m)
m = mirror(m)
for i in range(4):
    axarr[1,i].matshow(m, cmap=plt.cm.gray)
    m = rotate_right(m)

# Conclusion: the image is mirrored and rotated by 90°


#%% Visualise elastic deformations
from mnist_helper import elastic_transform

i = np.random.randint(len(y))
print i
i =2033

print "Sample labelled as", y[i]
m = -X[i,:].reshape((28,28))
m = mirror(rotate_right(m))
n = 5
f, axarr = plt.subplots(1,n)
axarr[0].matshow(m, cmap=plt.cm.gray)
for i in range(1,n):
    m2 = elastic_transform(m, kernel_dim=25, sigma=6, alpha=100, negated=False)
    m2 /= np.sqrt(np.sum(m2**2))
    axarr[i].matshow(m2, cmap=plt.cm.gray)
#print np.sum(m**2), np.sum(m2**2)


#%% Visualise deskewing
from mnist_helper import deskew

i = np.random.randint(len(y))
print "Sample labelled as", y[i]
m = -X[i,:].reshape((28,28))
f, axarr = plt.subplots(1,2)
axarr[0].matshow(m, cmap=plt.cm.gray)
axarr[1].matshow(deskew(m, (28,28)), cmap=plt.cm.gray)


#%% Gradient features

i = np.random.randint(len(y))
print "Sample labelled as", y[i]
img = -X[i,:].reshape((28,28))
sigma=1
rv = scipy.ndimage.filters.gaussian_filter1d(img, sigma=sigma, axis=0, 
                                             mode="reflect", order=1)
rh = scipy.ndimage.filters.gaussian_filter1d(img, sigma=sigma, axis=1, 
                                             mode="reflect", order=1)
m = np.sqrt(rv**2 + rh**2)
a = np.arctan2(rh, rv)

f, axarr = plt.subplots(1,5)
axarr[0].matshow(img, cmap=plt.cm.gray)
axarr[1].matshow(rv, cmap=plt.cm.gray)
axarr[2].matshow(rh, cmap=plt.cm.gray)
axarr[3].matshow(m, cmap=plt.cm.gray)
axarr[4].matshow(a, cmap=plt.cm.gray)


    