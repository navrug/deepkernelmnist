# -*- coding: utf-8 -*-
"""
Created on Mon Mar 14 15:43:50 2016

@author: gurvan
"""
from __future__ import division, absolute_import
import numpy as np


import scipy.ndimage.filters
import gaussian_filter
from scipy.ndimage.filters import gaussian_filter1d

import timeit

#%% Load data

X = np.loadtxt("Xtr.csv", delimiter=",")
d = len(X[0,:])
X = X*(X>0)
for i in xrange(len(X)):
    norm = np.dot(X[i,:],X[i,:])
    X[i,:] /= np.sqrt(norm)
    
i = 6
image = -X[i,:].reshape((28,28))

#%%

sigma = 0.5


t = timeit.time.time()
gy = gaussian_filter1d(image, sigma=sigma, axis=0, mode="reflect", order=1)
gx = gaussian_filter1d(image, sigma=sigma, axis=1, mode="reflect", order=1)
print 'SPICY convolution took %s microsec' %((timeit.time.time() - t)*1e8)

#t = timeit.time.time()
#gx2, gy2 = gaussian_filter_fast.deriv_gauss(image, sigma)
#print 'gaussian_filter_fast.deriv_gauss took %s sec' %(timeit.time.time()-t)


k1,k2 = separable_deriv_gauss_kernel(0.5)
t = timeit.time.time()
gx4 = fast_2D_convolve(image, k1, k2)
gy4 = fast_2D_convolve(image, k2, k1)
print 'Turbo numpy convolution %s microsec' %((timeit.time.time() - t)*1e8)

#print 'mean gx1: ', np.mean(gx1)
#print 'mean gx2: ', np.mean(gx2)
#print 'mean (|gx2 - gx1|): ', np.mean(np.abs(gx2 - gx1))
#print 'max |gx2 - gx1|: ', np.max(np.abs(gx2 - gx1))
#
#print 'mean gy1: ', np.mean(gy1)
#print 'mean gy2: ', np.mean(gy2)
#print 'mean (|gy2 - gy1|): ', np.mean(np.abs(gy2 - gy1))
#print 'max |gy2 - gy1|: ', np.max(np.abs(gy2 - gy1))
