# -*- coding: utf-8 -*-
"""
Created on Sat Mar  5 11:53:03 2016
"""

import numpy as np
import gaussian_filter
import math

from skimage import transform as tf
from numpy.random import random_integers



def rotate_right(m):
    result = np.zeros((len(m), len(m)))
    for i in range(28):
        for j in range(28):
            result[i,j] = m[j, len(m)-1-i]
    return result
    
def mirror(m):
    result = np.zeros((len(m), len(m)))
    for i in range(28):
        result[i,:] = m[len(m)-1-i,:]
    return result
    
def elastic_transform(image, kernel_dim=13, sigma=6, alpha=36, negated=False):
    # create an empty image
    result = np.zeros(image.shape)

    # create random displacement fields
    displacement_field_x = np.array([[random_integers(-1, 1) for x in xrange(image.shape[0])] \
                            for y in xrange(image.shape[1])]) * alpha
    displacement_field_y = np.array([[random_integers(-1, 1) for x in xrange(image.shape[0])] \
                            for y in xrange(image.shape[1])]) * alpha

    # create the gaussian kernel
    k1,k2 = gaussian_filter.separable_deriv_gauss_kernel(sigma, kernel_dim)

    #Gaussian kernel by applying twice k2
    displacement_field_x = gaussian_filter.fast_2D_convolve(displacement_field_x, k2, k2)
    displacement_field_y = gaussian_filter.fast_2D_convolve(displacement_field_y, k2, k2)

    # make the distorted image by averaging each pixel value to the neighbouring
    # four pixels based on displacement fields
    for row in xrange(image.shape[1]):
        for col in xrange(image.shape[0]):
            low_ii = row + int(math.floor(displacement_field_x[row, col]))
            high_ii = row + int(math.ceil(displacement_field_x[row, col]))

            low_jj = col + int(math.floor(displacement_field_y[row, col]))
            high_jj = col + int(math.ceil(displacement_field_y[row, col]))

            if low_ii < 0 or low_jj < 0 or high_ii >= image.shape[1] -1 \
               or high_jj >= image.shape[0] - 1:
                continue

            res = image[low_ii, low_jj]/4 + image[low_ii, high_jj]/4 + \
                    image[high_ii, low_jj]/4 + image[high_ii, high_jj]/4

            result[row, col] = res
    return result
    
def augment(X, y, f, size, kernel_dim=13, sigma=6, alpha=20):
    """
    Parameter explanation:
    - size
        - translation: size is the max translation size
        - elastic: size is the number of times we multiply the data
    """
    if f == "translation":
        X_augment = np.zeros(((2*size+1)*(2*size+1)*len(X), len(X[0,:])))
        y_augment = np.zeros((2*size+1)*(2*size+1)*len(X))
        k = 0
        for h in [-size,0,size]:    
            for v in [-size,0,size]: 
                translation = tf.SimilarityTransform(translation=(h, v))
                for i in xrange(len(X)):
                    X_augment[k*len(X) + i,:] = tf.warp(X[i, :].reshape(28,28), translation).flatten()
                    y_augment[k*len(X) + i] = y[i]
                k += 1
    if f == "elastic":
        X_augment = np.zeros((size*len(X), len(X[0,:])))
        y_augment = np.zeros(size*len(X))
        X_augment[:len(X), :] = X
        y_augment[:len(X)] = y
        for k in xrange(1,size):
                for i in xrange(len(X)):
                    X_augment[k*len(X) + i,:] =  elastic_transform(X[i, :].reshape(28,28), kernel_dim=kernel_dim, sigma=sigma, alpha=alpha).flatten()
                    y_augment[k*len(X) + i] = y[i]
                k += 1
    return X_augment, y_augment


def width_normalise(image, width):
    col_sum = np.sum(image, axis=0)
    nonzero = np.nonzero(col_sum)[0]
    start = nonzero[0]
    end = nonzero[-1]+1
    return tf.resize(image[:,start:end], (len(image), width))
    
def width_normalise_set(X, width):
    result = np.zeros((len(X), 28*width))
    for i in xrange(len(X)):
        result[i,:] = width_normalise(X[i,:].reshape(28,28), width).flatten()
    return result
    
