# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 20:57:50 2016
"""

import numpy as np

from gaussian_filter import fast_2D_convolve


def hog_histograms(gradient_columns, gradient_rows, cell_step, 
                   size_columns, size_rows, number_of_cells_columns, 
                   number_of_cells_rows, n_bins):

    m = np.hypot(gradient_columns, gradient_rows).flatten()
    ori = np.arctan2(gradient_rows, gradient_columns)*(180/np.pi) % 360
    ori = ori.flatten()
    
    cc = cell_step * number_of_cells_rows
    cr = cell_step * number_of_cells_columns
    bin_size = 360. / n_bins
    
    # Binning
    bins = np.zeros((cc*cr,n_bins))
    fst_bin = np.floor(np.divide(ori, bin_size)).astype(int)
    fst_bin[fst_bin == n_bins] = 0 # Account for rare precision errors
    bin_centers = (np.arange(n_bins)+0.5)*bin_size
    f = (ori - bin_centers[fst_bin])/bin_size
    snd_bin = np.mod(fst_bin + np.sign(f).astype(int), n_bins)
    bins[np.arange(cc*cr),fst_bin] += m*(1-np.abs(f))
    bins[np.arange(cc*cr),snd_bin] += m*np.abs(f)
    bins = bins.reshape(cr,cc,n_bins)
    
    cumsum = np.cumsum(np.cumsum(bins, axis=0), axis=1)
    cumsum = np.pad(cumsum, pad_width=((1,0),(1,0),(0,0)), mode="constant") 
    
    # Dynamic programming by using the fact that
    #   cumsum(i2,j2) + cumsum(i1,j1) - cumsum(i1,j2) - cumsum(i2,j1)
    # = sum(i1+1:i2, j1+1, j2)
    orientation_histogram = cumsum[cell_step::cell_step,cell_step::cell_step] \
        + cumsum[:-cell_step:cell_step,:-cell_step:cell_step] \
        - cumsum[cell_step::cell_step,:-cell_step:cell_step] \
        - cumsum[:-cell_step:cell_step,cell_step::cell_step]
    orientation_histogram /= (cell_step * cell_step)
    
    return orientation_histogram
#%%
#k1 k2  can be obtained with separable_deriv_gauss_kernel
def hog(image, k1, k2, n_bins=9, cell_step=4,
        block_step=2, sigma=0.3):

    image = np.atleast_2d(image)

#    gx = np.empty(image.shape, dtype=np.double)
#    gx[:, 0] = 0
#    gx[:, -1] = 0
#    gx[:, 1:-1] = image[:, 2:] - image[:, :-2]
#    gy = np.empty(image.shape, dtype=np.double)
#    gy[0, :] = 0
#    gy[-1, :] = 0
#    gy[1:-1, :] = image[2:, :] - image[:-2, :]

    gx = fast_2D_convolve(image, k1, k2)
    gy = fast_2D_convolve(image, k2, k1)

    sy, sx = image.shape
    n_cellsx = int(np.floor(sx // cell_step))  # number of cells in x
    n_cellsy = int(np.floor(sy // cell_step))  # number of cells in y

    # compute orientations integral images
    ###############################
    m = np.hypot(gx, gy).flatten()
    ori = np.mod(np.arctan2(gy, gx), 2*np.pi)
    ori = ori.flatten()
    
    cc = sy
    cr = sx
    bin_size = 360. / n_bins
    
    # Binning
    bins = np.zeros((cc*cr,n_bins))
    bin_size = 2*np.pi/n_bins
    fst_bin = np.floor(np.divide(ori, bin_size)).astype(int)
    fst_bin[fst_bin == n_bins] = 0 # Account for rare precision errors
    bin_centers = (np.arange(n_bins)+0.5)*bin_size
    f = (ori - bin_centers[fst_bin])/bin_size
    snd_bin = np.mod(fst_bin + np.sign(f).astype(int), n_bins)
    bins[np.arange(cc*cr),fst_bin] += m*(1-np.abs(f))
    bins[np.arange(cc*cr),snd_bin] += m*np.abs(f)
    bins = bins.reshape(cr,cc,n_bins)
    
    cumsum = np.cumsum(np.cumsum(bins, axis=0), axis=1)
    cumsum = np.pad(cumsum, pad_width=((1,0),(1,0),(0,0)), mode="constant") 
    
    # Dynamic programming by using the fact that
    #   cumsum(i2,j2) + cumsum(i1,j1) - cumsum(i1,j2) - cumsum(i2,j1)
    # = sum(i1+1:i2, j1+1, j2)
    orientation_histogram = cumsum[cell_step::cell_step,cell_step::cell_step] \
        + cumsum[:-cell_step:cell_step,:-cell_step:cell_step] \
        - cumsum[cell_step::cell_step,:-cell_step:cell_step] \
        - cumsum[:-cell_step:cell_step,cell_step::cell_step]
    orientation_histogram /= (cell_step * cell_step)
    ###############################

    # Block normalisation
    n_blocksx = (n_cellsx - block_step) + 1
    n_blocksy = (n_cellsy - block_step) + 1
    normalised_blocks = np.zeros((n_blocksy, n_blocksx,
                                  block_step, block_step, n_bins))
    for x in range(n_blocksx):
        for y in range(n_blocksy):
            block = orientation_histogram[y:y + block_step, x:x + block_step, :]
            eps = 1e-5
            normalised_blocks[y, x, :] = block / np.sqrt(block.sum()**2 + eps)

    return normalised_blocks.ravel()