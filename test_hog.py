# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 21:41:06 2016

@author: Ronan
"""

import matplotlib.pyplot as plt
import numpy as np
import hog
import hog2

import skimage.feature
from skimage import exposure

import timeit

#%% Load data

X = np.loadtxt("Xtr.csv", delimiter=",")
d = len(X[0,:])
X = X*(X>0)
for i in xrange(len(X)):
    norm = np.dot(X[i,:],X[i,:])
    X[i,:] /= np.sqrt(norm)
    
i = 6
image = -X[i,:].reshape((28,28))

#%% skimage.feature
t1 = timeit.time.time()

fd = skimage.feature.hog(image, orientations=8, 
                                    pixels_per_cell=(4, 4),
                                    cells_per_block=(1, 1))


print 'Test with skimage.feature took %s seconds' %(timeit.time.time() - t1)

#%% "homemade"
t1 = timeit.time.time()
fd = hog.hog(image, 
                        orientations=8, 
                        pixels_per_cell=(4, 4),
                        cells_per_block=(1, 1))


print 'Test with hog took %s seconds' %(timeit.time.time() - t1)


#%% equality test, without skimage.draw
t = timeit.time.time()
fd1 = skimage.feature.hog(image, 
                          orientations=8, 
                          pixels_per_cell=(4, 4),
                          cells_per_block=(1, 1), 
                          visualise=False)
print 'Test with skimage took %s seconds' %(timeit.time.time() - t)

t = timeit.time.time()
fd2 = hog.hog(image, 
              orientations=8, 
              pixels_per_cell=(4, 4),
              cells_per_block=(1, 1), 
              visualise=False)
print 'Test with hog took %s seconds' %(timeit.time.time() - t)

t = timeit.time.time()            
fd3 = hog2.hog(image, 
               orientations=8, 
               pixels_per_cell=(4, 4),
               cells_per_block=(1, 1), 
               visualise=False)
print 'Test with hog2 took %s seconds' %(timeit.time.time() - t)
            
print 'mean fd1: ', np.mean(fd1)
print 'mean fd2: ', np.mean(fd2)
print 'mean fd2: ', np.mean(fd3)
print 'mean (fd2 - fd1): ', np.mean(fd2 - fd1)
print 'max |fd2 - fd1|: ', np.max(np.abs(fd2 - fd1))
print 'mean (fd3 - fd1): ', np.mean(fd3 - fd1)
print 'max |fd3 - fd1|: ', np.max(np.abs(fd3 - fd1))

#### CONCLUSION:
#It works well, we can remove the visualisation part in hog.py, which uses
#skimage.draw (hence we don't need to import skimage at all).
#
#We should also change the code which is still a copy from skimage.