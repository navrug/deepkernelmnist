# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 17:56:39 2016

Kernel methods for machine learning - Kaggle competition
========================================================
Gurvan L'Hostis
Ronan Riochet
Arturo Ruano San Martin
Master MVA


IMPORTANT NOTE:
Our final submission (0.9810) used 25-fold data augmentation which makes
the SVM training 2-hour long.
We made a submission using the same parameters and only 9-fold augmentation
which is quicker to run and scored 0.9804.
"""

#AUGMENTATION_FACTOR = 9
AUGMENTATION_FACTOR = 25

###############################################################################
# Other parameters:
###############################################################################
# SVM
solver = 'smo'
kernel = 'poly'
degree = 4
offset = 1

# Elastic deformations
alpha = 20
sigma_el = 6

# HOG
n_bins = 14
sigma_hog = 0.3
###############################################################################


#%% Imports
import numpy as np
import timeit
import sys

np.random.seed(0)


#%% Data loading and preprocessing
from util import rotate_right, mirror

X = np.loadtxt("Xtr.csv", delimiter=",")
y = np.loadtxt("Ytr.csv", delimiter=",", usecols=[1], skiprows=1)
for i in xrange(len(X)):
    X[i,:] = mirror(rotate_right(X[i,:].reshape(28,28))).flatten()
X = X*(X>0)
X /= np.sqrt(np.sum(X**2, axis=1))[:,None]

X_test = np.loadtxt("Xte.csv", delimiter=",")
for i in xrange(len(X)):
    X[i,:] = mirror(rotate_right(X[i,:].reshape(28,28))).flatten()
X_test = X_test*(X_test>0)
X_test /= np.sqrt(np.sum(X_test**2, axis=1))[:,None]


#%% Model
from svm_classifier import svm_multiclass

svm = svm_multiclass(n_classes=10, 
                     C=2**(degree+1), 
                     degree=degree, 
                     offset=offset, 
                     gamma=2.0,
                     solver=solver,
                     kernel=kernel,
                     thresh=0,
                     max_iterations=1000,
                     verbose=False)


#%% Feature engineering
from hog import hog
from gaussian_filter import separable_deriv_gauss_kernel

k1,k2 = separable_deriv_gauss_kernel(sigma_hog, 13)

def sk_hog(x):
    img = x.reshape(28,28)
    h14 = hog(img, k1,k2,n_bins=n_bins, cell_step=14,
                    block_step=2, sigma=sigma_hog)
    h7 = hog(img, k1,k2,n_bins=n_bins, cell_step=7,
                    block_step=2, sigma=sigma_hog)
    h4 = hog(img, k1,k2,n_bins=n_bins, cell_step=4,
                    block_step=2, sigma=sigma_hog)
    return np.hstack([h4,2*h7,4*h14])
    
n_feat = 2576

X_odf = np.zeros((len(X), n_feat))
for i in xrange(len(X)):
    if np.mod(i,100) == 0:
        sys.stdout.write("\rTrain feature engineering advancement: %d%%" 
            %(100*float(i)/len(X)))
        sys.stdout.flush()
    X_odf[i,:] = sk_hog(X[i,:])
sys.stdout.write("\rTrain feature engineering advancement: 100%\n" )


X_test_odf = np.zeros((len(X_test), n_feat))
for i in xrange(len(X_test)):
    if np.mod(i,100) == 0:
        sys.stdout.write("\rTest feature engineering advancement: %d%%" 
            %(100*float(i)/len(X_test)))
        sys.stdout.flush()
    X_test_odf[i,:] = sk_hog(X_test[i,:])
sys.stdout.write("\rTest feature engineering advancement: 100%\n" )


#%% Training
from util import augment

clf = svm
start_time = timeit.default_timer()

# First pass with no augmentation
it_time = timeit.default_timer()
sys.stdout.write("Fitting the SVM..." )
clf.fit(np.hstack([X,X_odf]), y)
print "%s support vectors in %s min." \
    %(len(clf.support_), ((timeit.default_timer()-it_time) / 60.))
it_time = timeit.default_timer()

# Data augmentation
X_augment, y_augment = augment(X[clf.support_,:],y[clf.support_],
                               "elastic",size=AUGMENTATION_FACTOR,
                               sigma=sigma_el, alpha=alpha)
X_augment = X_augment*(X_augment>0)
X_augment /= np.sqrt(np.sum(X_augment**2, axis=1))[:,None]
X_augment_odf = np.zeros((len(X_augment), n_feat))
for i in xrange(len(X_augment)):
    if np.mod(i,100) == 0:
        sys.stdout.write("\rAugmented feature engineering advancement: %d%%" 
            %(100*float(i)/len(X_augment)))
        sys.stdout.flush()
    X_augment_odf[i,:] = sk_hog(X_augment[i,:])
sys.stdout.write("\rAugmented feature engineering advancement: 100%\n" )

# Second pass on augmented data
clf.fit(np.hstack([X_augment,X_augment_odf]), y_augment)

print "%s support vectors in %s min." \
    %(len(clf.support_), ((timeit.default_timer()-it_time) / 60.))


#%% Make submission
y_pred = clf.predict(np.hstack([X_test,X_test_odf]))

sub = np.arange(len(y_pred))+1
sub = np.vstack([sub, y_pred])
sub = sub.T
np.savetxt("poly4_elas25_deriv.csv", sub, fmt='%i', delimiter=",", 
           header="Id,Prediction", comments="")
